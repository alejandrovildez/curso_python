# Curso Python

## Instructores
Antoni Aloy y Bernat Cabezas 

---

### Enlaces de interés

https://gist.github.com/aaloy-tutor/10e4ebf4e1a071bd064c6198d00cfa0d

http://talks.apsl.net/python-sistemas/

https://github.com/APSL/curso-python-sistemas
 
---
### Requisitos
Git
```
sudo apt install git
which git
dpkg -l git
```
Pyenv
```sh
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc
exec "$SHELL"
pyenv install 3.6.9
//cat .bashrc
```
Pyenv as a plugin
```
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
exec "$SHELL"
```

Instalar versión de python
```
pyenv install 3.6.9 // x.y.z
```

Cambiar la versión de python
```
//pyenv global 3.6.9
pyenv local 3.6.9
```

Crear el env del proyecto
```
pyenv virtualenv <nombre_proyecto>
```

Instalar Fabric
```
$ sudo su -
# apt install python-pip python-jinja2
# pip2 install "fabric<2"

#checkout
<project>/code/fabric $ fab -l
#out
Available commands:

    hostname
    id
    lstmp
    sethostname
    uname
```

---

### Plugins para el OS
oh my zsh
https://github.com/ohmyzsh/ohmyzsh

pyenv
https://github.com/pyenv/pyenv

Beautiful Soup
parsear HTML

--

### Packages vistos
import json

import requests
https://realpython.com/python-requests/

import pdb; pdb.set_trace() // depurador?

import os

import pathlib

import re

import sqlite3

